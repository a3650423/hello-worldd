import java.util.Scanner;

public class HelloWorld{
    static void order(String food){
        System.out.println("You have ordered "+food+". Thank you!");
    }
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("What world you like to order:");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");
        System.out.print("Your order:");
        int number = userIn.nextInt();
        if (number == 1) {
            order("Tempura");
        } else if (number == 2) {
            order("Ramen");
        }
        else {
            order("Udon");


        }

        userIn.close();


    }
}
