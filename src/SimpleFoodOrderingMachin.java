import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachin {
    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order "+food+"?","Order Confirmation",JOptionPane.YES_NO_OPTION);
        if(confirmation==0) {
            receivedInfo.setText("Order for "+food+" received");
             JOptionPane.showMessageDialog(null,"Order for "+food+" received" );
        }
    }
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JTextPane receivedInfo;
    private JButton udonButton;

    public SimpleFoodOrderingMachin() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            order("Tempura");
            }

        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("Udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachin");
        frame.setContentPane(new SimpleFoodOrderingMachin().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
